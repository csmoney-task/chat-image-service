export default stream => new Promise((resolve) => {
  const chunks = [];

  stream.on('data', data => chunks.push(data));
  stream.on('end', () => resolve(Buffer.concat(chunks).toString()));
});
