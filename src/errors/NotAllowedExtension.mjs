import CustomError from './Custom';

export default class InvalidTypeError extends CustomError {
  constructor() {
    super('Not allowed image extension');
  }
}
