export default class CustomError extends Error {
  constructor(message) {
    super(message);

    this.name = this.constructor.name;

    if (Error.captureStackTrace !== undefined) {
      Error.captureStackTrace(this, this.constructor);
    } else {
      this.stack = (new Error(message)).stack;
    }
  }
}
