import CustomError from './Custom';

export default class InvalidTypeError extends CustomError {
  constructor() {
    super('Image not found');
  }
}
