import CustomError from './Custom';

export default class InvalidTypeError extends CustomError {
  constructor() {
    super('No image id specified');
  }
}
