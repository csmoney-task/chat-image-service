import CustomError from './Custom';

export default class InvalidTypeError extends CustomError {
  constructor() {
    super('Invalid file type (not image)');
  }
}
