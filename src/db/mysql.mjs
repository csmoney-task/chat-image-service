import { promisify } from 'util';
import mysql from 'mysql';

const {
  MYSQL_HOST: host,
  MYSQL_PORT: port,
  MYSQL_USER: user,
  MYSQL_PASSWORD: password,
  MYSQL_DATABASE: database,
} = process.env;

const pool = mysql.createPool({
  host,
  port,
  user,
  password,
  database,
});

pool.query = promisify(pool.query);
pool.queryRow = (q, p) => pool.query(q, p).then(r => r[0]);

export default pool;
