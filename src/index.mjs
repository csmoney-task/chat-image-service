import fs from 'fs';
import path from 'path';
import http from 'http';
import rootHandler from './handlers';

const { IMAGE_DOWNLOAD_DIR, PORT = '8081' } = process.env;

const miscImagesDir = path.resolve(IMAGE_DOWNLOAD_DIR, 'misc');

if (!fs.existsSync(miscImagesDir)) {
  fs.mkdirSync(miscImagesDir);
}

const server = http.createServer(rootHandler);

server.listen(PORT, () => console.log(`Image uploads server started on ${PORT}`));
