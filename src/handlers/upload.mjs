import fs from 'fs';
import path from 'path';
import sharp from 'sharp';
import nanoid from 'nanoid';
import multiparty from 'multiparty';
import { getStringFromStream } from '../utils';
import { InvalidTypeError } from '../errors';
import { mysql } from '../db';

const { IMAGE_DOWNLOAD_DIR } = process.env;

const getFormData = req => new Promise((resolve, reject) => {
  const form = new multiparty.Form();

  const result = {
    fields: {},
  };

  form.on('error', reject);

  form.on('part', async (part) => {
    part.on('error', reject);

    if (part.filename !== undefined) {
      if (!part.headers['content-type'].includes('image')) return reject(new InvalidTypeError());

      result.fileOriginalExtension = path.extname(part.filename);
      result.fileReadStream = part;
    } else {
      result.fields[part.name] = await getStringFromStream(part);
    }

    resolve(result);
  });

  form.parse(req);
});

const saveImageAndGetMeta = (readStream, downloadPath) => new Promise((resolve, reject) => {
  let meta;

  const writeStream = fs.createWriteStream(downloadPath);

  writeStream.on('error', reject);
  writeStream.on('close', () => resolve(meta));

  const pipeline = sharp().on('info', (info) => {
    meta = info;
  });

  readStream.pipe(pipeline).pipe(writeStream);
});

export default async (req, res) => {
  try {
    const { fields: { section = 'misc' }, fileReadStream, fileOriginalExtension } = await getFormData(req);

    const imageId = nanoid();

    const relativePath = path.join(section, `${imageId}${fileOriginalExtension}`);
    const downloadPath = path.resolve(IMAGE_DOWNLOAD_DIR, relativePath);

    const { height, width, format } = await saveImageAndGetMeta(fileReadStream, downloadPath);

    await mysql.query('INSERT INTO images SET ?', {
      extension: format,
      isOriginal: 1,
      relativePath,
      imageId,
      section,
      height,
      width,
    });

    res.json({ imageId });
  } catch (e) {
    console.error(e);

    if (e instanceof InvalidTypeError) {
      return res.throw(415, e);
    }

    res.throw(500, e);
  }
};
