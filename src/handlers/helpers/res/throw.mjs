const { NODE_ENV } = process.env;

export default res => (status = 500, value) => {
  res.writeHead(status, {
    'Content-Type': 'application/json',
  });

  const response = value instanceof Error
    ? {
      message: value.message,
      stack: value.stack,
    }
    : value;

  if (NODE_ENV === 'production') {
    response.stack = undefined;
  }

  res.end(JSON.stringify(response));
};
