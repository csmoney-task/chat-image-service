import getQueryHelper from './req/query';
import getJsonHelper from './res/json';
import getThrowHelper from './res/throw';
import getRedirectHelper from './res/redirect';

export const applyReqHelpers = req => Object.assign(req, {
  query: getQueryHelper(req),
});

export const applyResHelpers = res => Object.assign(res, {
  json: getJsonHelper(res),
  throw: getThrowHelper(res),
  redirect: getRedirectHelper(res),
});
