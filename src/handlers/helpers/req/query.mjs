import qs from 'querystring';

export default req => qs.parse(req.url.split('?')[1]);
