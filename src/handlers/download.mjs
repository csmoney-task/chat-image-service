import fs from 'fs';
import path from 'path';
import sharp from 'sharp';
import { mysql } from '../db';
import { NotAllowedExtensionError, NoIdSpecifiedError, NotFoundError } from '../errors';

const { IMAGE_SERVER_BASE_URL, IMAGE_DOWNLOAD_DIR } = process.env;

const allowedExtensions = ['jpg', 'png', 'webp'];

const getValidatedSettings = ({
  id,
  section = 'misc',
  width,
  height,
  isOriginal,
  extension = 'jpg',
}) => {
  if (id === undefined) {
    throw new NoIdSpecifiedError();
  }

  if (isOriginal === 'true') {
    return {
      isOriginal: true,
      section,
      id,
    };
  }

  if (!allowedExtensions.includes(extension)) {
    throw new NotAllowedExtensionError();
  }

  return {
    height: parseInt(height) || null,
    width: parseInt(width) || null,
    isOriginal: false,
    extension: `.${extension}`,
    section,
    id,
  };
};

const getOriginalImage = async (section, id) => {
  const data = await mysql.queryRow(`
    SELECT *
    FROM images
    WHERE section = ?
      AND imageId = ?
      AND isOriginal = 1
  `, [
    section,
    id,
  ]);

  if (data === undefined) {
    throw new NotFoundError();
  }

  return data;
};

const transformAndSaveImage = ({
  originalRelativePath,
  extension,
  imageId,
  section,
  height,
  width,
}) => new Promise((resolve, reject) => {
  const outputExtension = extension || path.extname(originalRelativePath);

  const outputFilename = [
    imageId,
    width !== null ? `_w${width}` : undefined,
    height !== null ? `_h${height}` : undefined,
    outputExtension,
  ].join('');

  const inputPath = path.resolve(IMAGE_DOWNLOAD_DIR, originalRelativePath);

  const outputRelativePath = path.join(section, outputFilename);
  const outputPath = path.resolve(IMAGE_DOWNLOAD_DIR, outputRelativePath);

  const transformer = sharp().resize({ width, height });

  const readStream = fs.createReadStream(inputPath);
  const writeStream = fs.createWriteStream(outputPath);

  readStream.on('error', reject);
  writeStream.on('error', reject);

  writeStream.on('finish', () => resolve(outputRelativePath));

  readStream.pipe(transformer).pipe(writeStream);
});

export default async (req, res) => {
  try {
    const {
      id, isOriginal, section, width, height, extension,
    } = getValidatedSettings(req.query);

    if (isOriginal) {
      const { relativePath } = await getOriginalImage(section, id);

      return res.redirect(`${IMAGE_SERVER_BASE_URL}/${relativePath}`);
    }

    const data = await mysql.queryRow(`
      SELECT *
      FROM images
      WHERE
        isOriginal = 0
        AND section = ?
        AND imageId = ?
        AND extension = ?
        AND width = ?
        AND height = ?
    `, [
      section,
      id,
      extension,
      width || 0,
      height || 0,
    ]);

    if (data !== undefined) {
      return res.redirect(`${IMAGE_SERVER_BASE_URL}/${data.relativePath}`);
    }

    const { imageId, relativePath: originalRelativePath } = await getOriginalImage(section, id);

    const newRalativePath = await transformAndSaveImage({
      originalRelativePath,
      extension,
      imageId,
      section,
      height,
      width,
    });

    await mysql.query('INSERT INTO images SET ?', {
      relativePath: newRalativePath,
      height: height || 0,
      width: width || 0,
      extension,
      imageId,
      section,
    });

    return res.redirect(`${IMAGE_SERVER_BASE_URL}/${newRalativePath}`);
  } catch (e) {
    console.error(e);

    if (e instanceof NotFoundError) {
      return res.throw(404, e);
    }

    res.throw(500, e);
  }
};
