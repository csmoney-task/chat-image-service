import { applyReqHelpers, applyResHelpers } from './helpers';
import download from './download';
import upload from './upload';

// only supports one level of nesting
const routesMap = new Map();

routesMap.set('download', download);
routesMap.set('upload', upload);

export default (req, res) => {
  applyReqHelpers(req);
  applyResHelpers(res);

  const route = req.url.split('?')[0].replace(/\//g, '');

  const handler = routesMap.get(route);

  if (handler === undefined) {
    return res.throw(404, {
      message: 'Not found',
    });
  }

  return handler(req, res);
};
