FROM node:10-alpine

WORKDIR /csmoney/chat-image-service

COPY package*.json ./

RUN npm i --production

COPY ./src ./

CMD [ \
    "node", \
    "--experimental-modules", \
    "index.mjs" \
]
