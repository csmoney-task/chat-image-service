#!/usr/bin/env bash

export PORT=8081
export NODE_ENV=development
export MYSQL_DATABASE=csmoney-chat


# manually defined variables:
# export MYSQL_HOST
# export MYSQL_USER
# export MYSQL_PASSWORD
# export IMAGE_DOWNLOAD_DIR
# export IMAGE_SERVER_BASE_URL

exec npx nodemon -q --inspect=9230 --experimental-modules src/index.mjs
